<?PHP
require 'database.php';
$score = get_score();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Worked example from lecture notes</title>
        <style>
            #keyboardCanvas
            {
                border:1px solid black;
                width:500px;
                height:500px;
            }

            #loadingMessage
            {
                position:absolute;
                top:100px;
                left:100px;
                z-index:100;
                font-size:50px;
            }
        </style>

        <script>
            var canvas;
            var g;
            var CANVAS_WIDTH = 500;
            var CANVAS_HEIGHT = 500;
            var box1 = 0;
            var box2 = 0;
            var box3 = 0;
            var score = 0;
            //Ending flag location
            var flagx = 400;
            var flagy = 260;
            var flagWidth = 69;
            var flagHeight = 200;
            var renderCanvasInterval = null;
            // checking the focus of the canvas
            var offFocus = false;
            var onFocus = false;
            //calculating fps
            var FPS;
            var previousFrameTime;
            // character position width and height
            var x = 10;
            var y = 400;
            var charWidth = 60;
            var charHeight = 70;
            var round = 1;
            var time = 0;
            
            //Location and size of the point boxes
            var x2 = 70;
            var x3 = 220;
            var x4 = 350;
            var y2 = 250;
            var y3 = 150;
            var y4 = 201;
            var pointWidth = 50;
            var pointHeight = 50;
            //character
            var character = new Image();
            character.src = "images/dog.png";
            //backgrounds
            var background1 = new Image();
            background1.src = "images/background1.jpg";
            var background2 = new Image();
            background2.src = "images/background2.png";
            var background3 = new Image();
            background3.src = "images/background3.jpe";
            //flag
            var flag = new Image();
            flag.src = "images/flag.png";
            //point boxs
            var point = new Image();
            point.src = "images/point.png";
            window.onload = onAllAssetsLoaded;
            document.write("<div id='loadingMessage'>Loading...</div>");

            function onAllAssetsLoaded()
            {
                // hide the webpage loading message
                document.getElementById('loadingMessage').style.visibility = "hidden";
                canvas = document.getElementById("keyboardCanvas");
                g = canvas.getContext("2d");
                canvas.width = CANVAS_WIDTH;
                canvas.height = CANVAS_HEIGHT;
                renderCanvas();
                canvas.addEventListener('mousemove', mousemoveHandler);
                document.addEventListener('keydown', keydownHandler);
                renderCanvasInterval = setInterval(renderCanvas, 10); // game loop

            }
            function renderCanvas()
            {
                if (round == 1) {
                    drawBackground1();
                }
                else if (round == 2) {
                    drawBackground2();
                  
                }
                else if (round == 3) {
                    drawBackground3();
                  
                }
                drawcharacter();
                drawFlag();
                isGameOver();
                points();
                points2();
                points3();
                trackPoints();
                isTargetHit();
                FPScalc();
                checkFocus();
                /* Continuously call requestAnimationFrame() to keep rendering the canvas */
                requestAnimationFrame(renderCanvas);
                displayTime();
            }

            function FPScalc() {
                // calculating the frames per milisecond
                var d = new Date();
                var n = d.getMilliseconds();
                FPS = Math.floor(1000 / (n - previousFrameTime));
                previousFrameTime = n;

                g.beginPath();
                g.fillStyle = "red";
                g.font = "30px Times Roman";
                g.fillText("FPS = " + FPS, 20, 25);
                g.closePath();

                //displaying warning when fps drops
                if (FPS < 30) {
                    document.getElementById("fps").innerHTML = "Warning low fps";
                }
                else {
                    document.getElementById("fps").innerHTML = "";
                }
            }

            function checkFocus() {
                // this function checks if you are in focus of the canvas, if you are not it will pause and reset the game.
                if (document.activeElement == document.getElementsByTagName("canvas")[0]) { //selects the canvas through the tag
                    onFocus = true;
                } else {
                    offFocus = true;
                    if (offFocus == true) {
                        clearInterval(renderCanvasInterval);
                        g.beginPath();
                        g.fillStyle = "red";
                        g.font = "200px Times Roman";
                        g.fillText("Pause!", 20, 300);
                        g.closePath();
                        round = 1;
                        x = 10;
                        y = 400;
                        targetDestroyed = false;
                        time = 0;
                    }
                }
                timer();
            }
            // setting an interval to check the focus ever 10 seconds
            window.setInterval(checkFocus, 10);

            //creating the timer function. this has an internval that calls it every 1 second. therefore it add 1 to the timer every second. this creates the timer in the game
            function timer()
            {
                time++;
            }
            function displayTime() {
                //displaying the time 
                g.beginPath();
                g.fillStyle = "red";
                g.font = "30px Times Roman";
                g.fillText("Time = " + time, 50, 50);
                g.closePath();
            }

            function trackPoints()
            {
                g.beginPath();
                g.fillStyle = "red";
                g.font = "30px Times Roman";
                g.fillText("Points = " + score, 200, 20);
                g.closePath();
            }
            function points()
            {
                g.drawImage(point, x2, y2, pointWidth, pointHeight);
            }
            function points2()
            {
                g.drawImage(point, x3, y3, pointWidth, pointHeight);
            }
            function points3()
            {
                g.drawImage(point, x4, y4, pointWidth, pointHeight);
            }
            function drawFlag()
            {
                g.drawImage(flag, flagx, flagy, flagWidth, flagHeight);
            }


            //checking to see if any of the targets have been hit
            function isTargetHit()
            {
                //box 1
                if ((x >= x2) && (x <= (x2 + pointWidth))
                        && (y >= y2) && (y <= (y2 + pointHeight)))
                {
                    //giving out points
                    score = score + 10;
                    //making the player go to the floor
                    y = 400;


                }
                //box 2
                if ((x >= x3) && (x <= (x3 + pointWidth))
                        && (y >= y3) && (y <= (y3 + pointHeight)))
                {
                    //giving out points
                    score = score + 10;
                    //making the player go to the floor
                    y = 400;
                }

                //box 3
                if ((x >= x4) && (x <= (x4 + pointWidth))
                        && (y >= y4) && (y <= (y4 + pointHeight)))
                {
                    //giving out points                     
                    score = score + 10;
                    //making the player go to the floor
                    y = 400;
                }
            }

            function drawcharacter()
            {
                g.drawImage(character, x, y, charWidth, charHeight);
            }
            function drawBackground1() {
                g.drawImage(background1, 0, 0, canvas.width, canvas.height);
            }
            function drawBackground2()
            {
                g.drawImage(background2, 0, 0, canvas.width, canvas.height);
            }
            function drawBackground3()
            {
                g.drawImage(background3, 0, 0, canvas.width, canvas.height);
            }
            function keydownHandler(e)
            {
                var stepSize = 10;
                if (e.keyCode === 37)  // left
                {
                    x -= stepSize;
                }
                else if (e.keyCode === 38) // up
                {
                    y -= stepSize;
                }
                else if (e.keyCode === 39) // right
                {
                    x += stepSize;
                }
                else if (e.keyCode === 40) // down
                {
                    y += stepSize;
                }
                // this loop prevents the character from leaving the map
                //so that the player cant go lower and off range of the canvas, it resets the character position when it goes beyond a certain range 
                if (y > 400)
                {
                    // stops you from going lower
                    y = 400;

                }
                else if (y < 150)
                {
                    // stops you from going higher than the canvas range 
                    y = 400;
                }
                if (x <= 0)
                {
                    //  stops you from going backwards out of canvas range
                    x = 1;

                }
                else if (x > 440)
                {
                    // stops you from going forwards, out of canvas range
                    x = 440;

                }
            }
            function mousemoveHandler(e)
            {
                var stepSize = 10;
                if (e.which == 1)  // left mouse button
                {
                    if (e.x > x)
                    {
                        x += stepSize;
                    }
                    else
                    {
                        x -= stepSize;
                    }
                }
            }
            //function to tell whether the player has touched the end flag yet, and sees how many point they have
            function isGameOver()
            {
                if ((x >= flagx) && (x <= (flagx + flagWidth)) && (y >= flagy) && (y <= (flagy + flagHeight)))
                {
                    //this loop only allows players to progress to the next level if they get the right score 
                    if (score >= 10 && round == 1)
                    {
                        alert("You have reached the end of the level with " + score + " points!\nYou win!");
                        round = 2;
                        x = 10;
                        y = 400;
                       
                        renderCanvas();

                    }
                    else if (score >= 20 && round == 2)

                    {
                        alert("You have reached the end of the level with " + score + " points!\nYou win!");
                        round = 3;
                        x = 10;
                        y = 400;
                        
                        renderCanvas();
                    }
                    else if (score >= 30 && round == 3)
                    {
                        g.beginPath();
                        g.fillStyle = "blue";
                        g.font = "300px Times Roman";
                        g.fillText("Win!", 1, 250);
                        g.closePath();
                    }
                    /* else if (score < 10 && round != 1){
                     alert("You have reached the end of the level with " + score + " points!\nYou lose!");
                     clearInterval(renderCanvasInterval);
                     g.beginPath();
                     g.fillStyle = "red";
                     g.font = "200px Times Roman";
                     g.fillText("Lose!", 90, 300);
                     g.closePath();
                     } */

                }
            }


        </script>
    </head>
    <body>

        <canvas id = "keyboardCanvas" tabindex="1"   >
            Your browser does not support the HTML5 'Canvas' tag.
        </canvas>
        <p>Use the four arrow keys to move the rectangle.</p>
        <p id="fps"></p>

        <h1>High scores!</h1>
        <p>
            <?PHP
            foreach ($score as $x) {
                echo $x['highestScore'] // using s foreach to display score from database
                ?> <br>
            <?PHP } ?>
        </p>

    </body>
</html>
