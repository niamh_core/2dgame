<?php

$dsn = 'mysql:host=localhost;dbname=score';
$username = 'root';
$password = '';

try {
    $db = new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    $error_message = $e->getMessage();
    include('../errors/database_error.php');
    exit();
}

function get_score() {
    global $db;
    $query = 'SELECT highestScore FROM high_score order by highestScore DESC';
    $statement = $db->prepare($query);
  
    $statement->execute();
    $score = $statement->fetchAll();
    $statement->closeCursor();
    $highestScore = $score;
    return $highestScore;
}

function add_score($score) {
    global $db;
    $query = 'INSERT INTO `high_score`(`highestScore`) VALUES (:score)';
    $statement = $db->prepare($query);
    $statement->bindValue(':score', $score);
    $statement->execute();
    $statement->closeCursor();
}
?>